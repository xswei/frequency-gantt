let data = [
  {
    XTMC: '系统1',
    PLXX: '30MHz',
    PLSX: '90MHz',
    SSPT: '平台1',
    SSDY: 'A01单元',
    SSXD: '行动1',
    CTPD: [
      ['30Mhz', '50Mhz']
    ]
  }, {
    XTMC: '系统2',
    PLXX: '80MHz',
    PLSX: '150MHz',
    SSPT: '平台1',
    SSDY: 'A01单元',
    SSXD: '行动1',
    CTPD: [
      ['120Mhz', '150Mhz']
    ]
  }, {
    XTMC: '系统3',
    PLXX: '500MHz',
    PLSX: '700MHz',
    SSPT: '平台2',
    SSDY: 'A01单元',
    SSXD: '行动1',
    CTPD: [
      ['500Mhz', '600Mhz']
    ]
  }, {
    XTMC: '系统4',
    PLXX: '2GHz',
    PLSX: '12GHz',
    SSDY: 'A01单元',
    SSXD: '行动1',
    CTPD: [
      ['2GHz', '3GHz'],
      ['2GHz', '5GHz'],
      ['10GHz', '12GHz']
    ]
  }, {
    XTMC: '系统5',
    PLXX: '120MHz',
    PLSX: '600MHz',
    SSPT: '平台3',
    SSDY: 'A02单元',
    SSXD: '行动1',
    CTPD: [
      ['120MHz', '150MHz'],
      ['500MHz', '600MHz']
    ]
  }, {
    XTMC: '系统6',
    PLXX: '800MHz',
    PLSX: '5GHz',
    SSPT: '平台3',
    SSDY: 'A02单元',
    SSXD: '行动1',
    CTPD: [
      ['1GHz', '3GHz'],
      ['2GHz', '5GHz']
    ]
  }, {
    XTMC: '系统7',
    PLXX: '80GHz',
    PLSX: '120GHz',
    SSDY: 'A02单元',
    SSXD: '行动1',
    CTPD: [
      ['80GHz', '120GHz']
    ]
  }, {
    XTMC: '系统8',
    PLXX: '3MHz',
    PLSX: '50MHz',
    SSPT: '平台4',
    SSDY: 'A03单元',
    SSXD: '行动2',
    CTPD: [
      ['30MHz', '50MHz']
    ]
  }, {
    XTMC: '系统9',
    PLXX: '1GHz',
    PLSX: '3GHz',
    SSPT: '平台4',
    SSDY: 'A03单元',
    SSXD: '行动2',
    CTPD: [
      ['1GHz', '3GHz'],
      ['2GHz', '3GHz']
    ]
  }, {
    XTMC: '系统10',
    PLXX: '10GHz',
    PLSX: '500GHz',
    SSDY: 'A03单元',
    SSXD: '行动2',
    CTPD: [
      ['10GHz', '12GHz'],
      ['80GHz', '120GHz']
    ]
  }
]

module.exports = data
