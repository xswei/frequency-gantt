var config = require('./config')
var utils = require('./utils')

var gantt = {}

Object.keys(utils).forEach(function (_u) {
  gantt[_u] = utils[_u]
})

gantt.init = function ({el, option, d3, data}) {
  this.config = this._getOptions(config, option)
  this.size = this._getElementSize(el)
  this.el = el
  this.d3 = d3
  this.originData = data  // 原始数据  不做修改
  this.hierarchyData = this._formatData(JSON.parse(JSON.stringify(data))) // 层次数据
  this.flatData = this._getListByHierarchy(this.hierarchyData)
  console.log(this)
  this.initSvg()
  return this
}

gantt.initSvg = function () {
  var me = this
  var d3 = me.d3
  me.svg = d3.select(me.el).append('svg')
    .attr('width', me.size.width)
    .attr('height', me.size.height)
  me.scale = d3.scalePow()
    .exponent(0.2)
    .domain(me.config.frequencyRange)
    .range([
      me.config.margin.left,
      me.size.width - me.config.margin.right
    ])
  var xAxisG = me.svg.append('g')
    .attr('transform', 'translate(0, ' + me.config.margin.top + ')')
    .attr('class', 'x-aixs axis')
  xAxisG.append('path').attr('d', function () {
    return 'M' + me.config.margin.left +',0H' + (me.size.width - me.config.margin.right)
  }).attr('stroke', 'black')
  xAxisG.selectAll('line.split').data(me.config.segments)
    .enter().append('line').attr('class', 'split')
    .attr('x1', function (d) {
      return me.scale(d)
    })
    .attr('y1', -10)
    .attr('x2', function (d) {
      return me.scale(d)
    })
    .attr('y2', 5)
    .attr('stroke', 'black')
  let pairs = d3.pairs(me.config.segments)
  xAxisG.selectAll('text.label')
    .data(pairs).enter()
    .append('text').attr('class', 'label')
    .attr('x', function (d) {
      return (me.scale(d[0]) + me.scale(d[1])) / 2
    })
    .attr('text-anchor', 'middle')
    .attr('y', 10)
    .attr('dy', '-.7em')
    .text(function (d) {
      return me._convertToUnit(d[0]) + '-' + me._convertToUnit(d[1])
    })
  console.log(pairs)
  // me.svg.append('g')
  //   .attr('transform', 'translate(0, ' + me.config.margin.top + ')')
  //   .call(d3.axisTop(me.scale))
}

module.exports = gantt
