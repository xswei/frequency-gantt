var config = {
  normal: 'steelblue',
  cover: 'red',
  padding: 10,
  idStr: '_noParent',
  frequencyRange: [0, 1000 * 1000],
  segments: [0, 30, 300, 3000, 30000, 300000, 1000000],
  minHeight: 1000,
  minWidth: 1000,
  margin: {
    left: 200,
    top: 20,
    bottom: 20,
    right: 20
  }
}

module.exports = config
