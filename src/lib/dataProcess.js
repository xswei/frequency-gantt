function process (d3, data) {
  var nestData = d3.nest()
    .key(function (d) {
      return d.SSXD
    })
    .key(function (d) {
      return d.SSDY
    })
    .key(function (d) {
      return d.SSPT
    })
    .entries(data)
  console.log(nestData)
}

module.exports = process
