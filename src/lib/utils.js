function _getOptions (_default, _custom) {
  var _option = _default
  if (typeof _custom === 'object') {
    Object.keys(_custom).forEach(function (_c) {
      _option[_c] = _custom[_c]
    })
  }
  return _option
}

function _convertToUnit (_f) {
  // 不带单位的转为带单位的表示
  if (_f === 0) return _f
  if (_f < 1000) {
    return _f
  } else {
    return _f / 1000 + 'GHz'
  }
}

function _convertByUnit (_u) {
  // 带单位的转为以MHz为单位的数值
  let num = parseInt(_u)
  if (_u.match(/mhz/i)) {
    // .
  } else if (_u.match(/ghz/i)) {
    num *= 1000
  }
  return num
}

function _getListByHierarchy (_root) {
  var me = this
  var d3 = me.d3
  var list = []
  let _r = d3.hierarchy(_root, function (d) {
    return d.values
  }).eachBefore(function (d) {
    list.push(d)
  })
  list = list.filter(function (d) {
    return d.depth !== 0 && d.height !== 0
  })
  list.forEach(function (d) {
    if (d.data.key === me.config.idStr) {
      console.log(d)
    }
  })
  return list
  // console.log(_r)
}

function _formatData (data) {
  var me = this
  var d3 = me.d3
  console.log(data)
  var nestData = d3.nest()
    .key(function (d) {
      return d.SSXD || me.config.idStr
    })
    .key(function (d) {
      return d.SSDY || me.config.idStr
    })
    .key(function (d) {
      return d.SSPT || me.config.idStr
    })
    .key(function (d) {
      return d.XTMC || me.config.idStr
    })
    .entries(data)
  return {
    name: 'root',
    values: nestData
  }
}

function _getElementSize (el) {
  var me = this
  if (!el) {
    throw new Error('必须指定一个容器元素')
  }
  var computed = getComputedStyle(el)
  var w = parseInt(computed.width)
  var h = parseInt(computed.height)
  return {
    width: Math.max(w, me.config.minWidth),
    height: Math.max(h, me.config.minHeight)
  }
}

module.exports = {
  _getOptions,
  _getListByHierarchy,
  _formatData,
  _getElementSize,
  _convertByUnit,
  _convertToUnit
}
